import React, { useEffect, useState } from "react";
import CustomerDataService from "../services/customer.service";
import { Link } from "react-router-dom";

const CustomerList = () => {
  const [customerList, setCustomerList] = useState([]);
  const [selectedCustomer, setSelectedCustomer] = useState(null);
  const [currentIndex, setCurrentIndex] = useState(-1);
  const [searchParams, setSearchParams] = useState("");

  const retrieveCustomers = () => {
    CustomerDataService.getAll()
      .then((response) => {
        setCustomerList(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    if (customerList.length) return;
    retrieveCustomers();
  }, [retrieveCustomers, customerList]);

  const onChangeSearchParams = (e) => {
    const searchTitle = e.target.value;
    setSearchParams(searchTitle);
  };

  const refreshList = () => {
    retrieveCustomers();
    setCurrentIndex(-1);
    setSelectedCustomer(null);
  };

  const handleSelectedCustomer = (customer, index) => {
    setCurrentIndex(index);
    setSelectedCustomer(customer);
  };

  const removeAllCustomers = () => {
    CustomerDataService.deleteAll()
      .then((response) => {
        refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const searchTitle = () => {
    setCurrentIndex(-1);
    setSelectedCustomer(null);

    CustomerDataService.findByFirstName(searchParams)
      .then((response) => {
        setCustomerList(response.data);
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  return (
    <div className="list row">
      <div className="col-md-8">
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Search by customer"
            value={searchParams}
            onChange={onChangeSearchParams}
          />
          <div className="input-group-append">
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={searchTitle}
            >
              Search
            </button>
          </div>
        </div>
      </div>
      <div className="col-md-6">
        <h4>Customers List</h4>

        <ul className="list-group">
          {customerList &&
            customerList.map((customer, index) => (
              <li
                className={
                  "list-group-item " + (index === currentIndex ? "active" : "")
                }
                onClick={() => handleSelectedCustomer(customer, index)}
                key={index}
              >
                {customer.firstName} {customer.lastName}
              </li>
            ))}
        </ul>

        <button
          className="m-3 btn btn-sm btn-danger"
          onClick={removeAllCustomers}
        >
          Remove All
        </button>
      </div>
      <div className="col-md-6">
        {selectedCustomer ? (
          <div>
            <h4>Customer</h4>
            <div>
              <label>
                <strong>First Name:</strong>
              </label>{" "}
              {selectedCustomer.firstName}
            </div>
            <div>
              <label>
                <strong>Last Name:</strong>
              </label>{" "}
              {selectedCustomer.lastName}
            </div>
            <div>
              <label>
                <strong>Email:</strong>
              </label>{" "}
              {selectedCustomer.email}
            </div>
            <div>
              <label>
                <strong>Phone Number:</strong>
              </label>{" "}
              {selectedCustomer.phoneNumber}
            </div>
            <div>
              <label>
                <strong>Post Code:</strong>
              </label>{" "}
              {selectedCustomer.postCode}
            </div>

            <Link
              to={"/customers/" + selectedCustomer.id}
              className="badge badge-warning"
            >
              Edit
            </Link>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Customer...</p>
          </div>
        )}
      </div>
    </div>
  );
};
export default CustomerList;
