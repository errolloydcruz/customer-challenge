# Customer Challenge

## Getting started

### `run npm install`

### `run npm start` for the React App

### `run node server.js` for the Node server

### About the challenge:

As I was developing and building different kind of React applications this one also helps me to go back from where I started.
It's like looking back for the things I used to write when I was starting to learn React and it helps me to be humbled that with this kind of challenges it reminds me of all the learnings I gained through the years of experience being a developer.

This was a great chance for me to be able to create a simple node express server for a simple react app.
It's challenging yet satisfying that learning new things with small steps one at a time is achievable.

I searched and learned the basic configurations of setting up a node express server for this one. I tried to debug from the client side to the server side how things are going on and how things are handled using this technology.
