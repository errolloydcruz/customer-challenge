# Getting Started with Node Express Server

## Available Scripts

In the project directory, you can run:

### npm install

Runs for installing app dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:8080](http://localhost:8080) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### About the challenge:

This was a great chance for me to be able to create a simple node express server for a simple react app.
It's challenging yet satisfying that learning new things with small steps one at a time is achievable.

I searched and learned the basic configurations of setting up a node express server for this one. I tried to debug from the client side to the server side how things are going on and how things are handled using this technology.
